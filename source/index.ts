
import * as http from "http";
import * as querystring from "querystring";
import { print } from "./lib";

const port = 8080;
const server = http.createServer();

server.on('request', function (req: any, res: any) {
    if (req.method == 'POST') {
        var body = '';
    }

    req.on('data', function (data: any) {
        body += data;
    });

    req.on('end', function () {
        var post = querystring.parse(body);
        print(post);
        res.writeHead(200, { 'Content-Type': 'text/plain; charset=UTF-8' });
        res.end('Hello World\n');
    });
});

server.listen(port);

print(`Listening on port ${port}`);
