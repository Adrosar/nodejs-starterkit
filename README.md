## Instalacja

1. Zainstaluj **Node.js**. Zalecam wersję: _LTS v6.11.3 (includes npm 3.10.10)_
2. Otwórz konsolę _(terminal)_ i przejdź do folderu projektu
3. Zainstaluj menadżer pakietów [YARN](https://yarnpkg.com), poleceniem:

        npm install -g yarn

4. Zainstaluj zależności z pliku **package.json**

        yarn install

5. Gotowe :)


## Zadania

Wszystkie skrypty **ZADAŃ** znajdują się w folderze **bin**.
Polecenie uruchamiające konkretne zadanie należn wykonać w konsoli _(terminalu)_ będąc w katalogu projektu.

- Interaktywny tryb _(czuwaj i buduj)_: `yarn run watch`
- Budowanie aplikacji: `yarn run build`
- Uruchomienie aplikacji w trybie deweloperskim: `yarn run server`


### Uruchomienie aplikacji
Aplikacja jest uruchamiana za pośrednictwem narzędzia [nodemon](https://nodemon.io). Będzie ona działać tylko pod adresem `127.0.0.1:8080`. Można to zmienić w pliku `./bin/server.js`.


## Struktura projektu

Foldery _(katalogii)_:

- `./bin` - Skrypty **ZADAŃ**.
- `./build` - Zbudowana _(skompilowana)_ aplikacja.
- `./node_modules` - Paczki z **NPM** i **YARN**.
- `./source` - Źródło aplikacji _(Backend w Node.js)_.
- `./typings` - Własne pakiety z **typami** globalnymi.
