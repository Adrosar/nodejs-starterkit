
const path = require('path');
const projectDir = path.resolve(__dirname, "..");
const tscExec = path.resolve(projectDir, "node_modules", "typescript", "lib", "tsc.js");

process.chdir(projectDir);
process.argv.push("--watch");

require(tscExec);
