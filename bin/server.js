
const path = require('path');
const cp = require('child_process');

const projectDir = path.resolve(__dirname, "..");
const serverIndex = path.resolve(projectDir, "build", "index.js");
const nodemonExec = path.resolve(projectDir, "node_modules", "nodemon", "bin", "nodemon.js");

process.chdir(projectDir);
process.argv.push(serverIndex);

require(nodemonExec);
